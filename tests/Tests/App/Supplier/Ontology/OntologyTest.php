<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\App\Provider;

use App\Service\Ontology\Storer\StorerService;
use App\Supplier\Ontology\Datatourisme;
use PHPUnit\Framework\TestCase;

/**
 * Lupin's providers rely on multiple distant RDF sources. It is important to test fiability of these servers before deploying the application and trying to cache ontologies.
 * @package Tests\App\Supplier\Ontology
 */
class SupplierTest extends TestCase
{
    /**
     * Ontology Service
     * @var $storerService
     */
    private static $storerService;

    /**
     * SetUp
     */
    public static function setUpBeforeClass()
    {
        self::$storerService = new StorerService(
            array(
                new Datatourisme(false)
            ),
            array(
                "cache_folder" => "../var/cache"
            )
        );
    }

    /**
     * Tests Ontology's distant source.
     */
    public function testOntologyProvider()
    {
        $provider = self::$storerService->getSupplier("Datatourisme");
        $ontology = json_decode($provider->getOntology());

        $this->assertEquals(JSON_ERROR_NONE, json_last_error(), "Provided ontology must be valid JSON.");
    }
}