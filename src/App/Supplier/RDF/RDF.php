<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Supplier\RDF;

use App\Service\Ontology\RDFDefinitions\RDFDefinitions;

/**
 * Class RDF
 * @package App\Supplier\RDF
 */
class RDF extends RDFDefinitions
{
    const MAIN_ID_URI       = 'http://purl.org/dc/elements/1.1/identifier';
    const MAIN_LANG_URI     = 'https://www.datatourisme.gouv.fr/ontology/core#availableLanguage';
    const MAIN_PRIORITY_URI = 'https://www.datatourisme.gouv.fr/ontology/core#hasPriority';

    const DATATYPE_LITERAL  = 'http://www.w3.org/2000/01/rdf-schema#Literal';
}