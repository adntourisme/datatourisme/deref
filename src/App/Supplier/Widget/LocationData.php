<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Supplier\Widget;

use App\Service\Display\Widget\AbstractWidgetData;

/**
 * Class LocationData
 * @package App\Supplier\Widget
 */
class LocationData extends AbstractWidgetData
{
    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @var string|null
     */
    private $postalCode;

    /**
     * @var string|null
     */
    private $addressLocality;

    /**
     * @var string|null
     */
    private $streetAddress;

    /**
     * LocationData constructor.
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;

        parent::__construct(Location::class);
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return self
     */
    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return self
     */
    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     * @return self
     */
    public function setPostalCode(string $postalCode = null): self
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddressLocality()
    {
        return $this->addressLocality;
    }

    /**
     * @param string|null $addressLocality
     * @return self
     */
    public function setAddressLocality(string $addressLocality = null): self
    {
        $this->addressLocality = $addressLocality;
        return $this;
    }

    /**
     * @param string $streetAddress
     * @return string|null
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * @param string|null $streetAddress
     * @return self
     */
    public function setStreetAddress(string $streetAddress = null): self
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }
}