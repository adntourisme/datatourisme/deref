<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Supplier\Widget;

use App\Service\Display\Widget\AbstractWidget;
use App\Service\Display\Widget\AbstractWidgetData;
use App\Sparql\SparqlClient;
use App\Supplier\RDF\RDF;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Location
 * @package App\Widget
 */
class Location extends AbstractWidget
{
    /**
     * {@inheritdoc}
     */
    public function __construct(string $templateFile = "app/widgets/location.twig", bool $uniqueProperties = false)
    {
        parent::__construct($templateFile, $uniqueProperties);
    }

    /**
     * {@inheritdoc}
     */
    public function isAvailable(): bool
    {
        if (!$this->graph)
            throw new \Exception(
                "Widget must be populated with dereferenced RDFGraph" .
                "before being candidate for display availability.");

        return null !== $this->graph->getMasterResource()->getResource(
            "https://www.datatourisme.gouv.fr/ontology/core#isLocatedAt"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        if ($this->data instanceof AbstractWidgetData)
            return $this->data;

        $config = Yaml::parse(file_get_contents('../app/config/parameters.yml'));
        $sparqlEndpoint = $config['parameters']['suppliers']['widgets']['location']['endpoint'] ?? null;
        $sparqlFallbackEndpoints = $config['parameters']['suppliers']['widgets']['location']['fallback_endpoints'] ?? null;

        if (!$sparqlEndpoint)
            return null;

        $uri = $this->graph->getUri();

        $selects = array(
            'postalCode'      => '?postalCode',
            'addressLocality' => '?addressLocality',
            'streetAddress'   => '?streetAddress',
            'cityLabel'       => '?cityLabel',
            'latitude'        => '?latitude',
            'longitude'       => '?longitude',
        );

        $query = "
            prefix : <https://www.datatourisme.gouv.fr/ontology/core#>
            prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            prefix schema: <http://schema.org/>

            SELECT ".implode(" ", $selects)."
            WHERE
            {
              <$uri>        :isLocatedAt             ?locNode          .
              ?locNode      schema:geo               ?geoNode          .
              ?geoNode      schema:latitude          ?latitude         .
              ?geoNode      schema:longitude         ?longitude        .
              OPTIONAL
              {
                           ?locNode           schema:address           ?postalNode          .
                           ?postalNode        :hasAddressCity          ?addressLocality     .
                OPTIONAL { ?postalNode        schema:postalCode        ?postalCode       }  .
                OPTIONAL { ?postalNode        schema:streetAddress     ?streetAddress    }  .
                OPTIONAL { ?addressLocality   rdfs:label               ?cityLabel        }  .
              }
            }
        ";

        $client = new SparqlClient($sparqlEndpoint, $sparqlFallbackEndpoints);
        $client->setHeaders(["Accept: application/json"]);
        $result = $client->query($query);
        $result = json_decode($result, true);

        if (!isset($result['results']['bindings']) || empty($result['results']['bindings']))
            return null;

        $data = array();
        $bindings = &$result['results']['bindings'];  // Just for variable readability
        for ($i = 0, $count = count($bindings); $i < $count; ++$i)
            foreach ($bindings[$i] as $var => $values)
                $data[array_search("?$var", $selects)] = $values['value'] ?? null;

        if (!isset($data['latitude']) || !isset($data['longitude']))
            return null;

        $locationData = new LocationData((float) $data['latitude'], (float) $data['longitude']);
        $locationData->setPostalCode($data['postalCode'] ?? null);
        $locationData->setAddressLocality($data['cityLabel'] ?? $data['addressLocality'] ?? null);
        $locationData->setStreetAddress($data['streetAddress'] ?? null);

        $this->setData($locationData);

        return $this->getData();
    }
}