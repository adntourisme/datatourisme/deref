<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\RDFDefinitions;

/**
 * Class RDFDefinitions
 * @package App\Service\Ontology\RDFDefinitions
 */
class RDFDefinitions
{
    // ____ MAIN URIs
    const MAIN_COMMENT_URI               =   'http://www.w3.org/2000/01/rdf-schema#comment';
    const MAIN_LABEL_URI                 =   'http://www.w3.org/2000/01/rdf-schema#label';
    const MAIN_TYPE_URI                  =   'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';

    // ____ DATATYPES
    const DATATYPE_BOOLEAN               =   'http://www.w3.org/2001/XMLSchema#boolean';
    const DATATYPE_BYTE                  =   'http://www.w3.org/2001/XMLSchema#byte';
    const DATATYPE_DATE                  =   'http://www.w3.org/2001/XMLSchema#date';
    const DATATYPE_DATE_TIME             =   'http://www.w3.org/2001/XMLSchema#dateTime';
    const DATATYPE_DECIMAL               =   'http://www.w3.org/2001/XMLSchema#decimal';
    const DATATYPE_DOUBLE                =   'http://www.w3.org/2001/XMLSchema#double';
    const DATATYPE_FLOAT                 =   'http://www.w3.org/2001/XMLSchema#float';
    const DATATYPE_INT                   =   'http://www.w3.org/2001/XMLSchema#int';
    const DATATYPE_INTEGER               =   'http://www.w3.org/2001/XMLSchema#integer';
    const DATATYPE_LONG                  =   'http://www.w3.org/2001/XMLSchema#long';
    const DATATYPE_NEGATIVE_INTEGER      =   'http://www.w3.org/2001/XMLSchema#negativeInteger';
    const DATATYPE_NON_NEGATIVE_INTEGER  =   'http://www.w3.org/2001/XMLSchema#nonNegativeInteger';
    const DATATYPE_NON_POSITIVE_INTEGER  =   'http://www.w3.org/2001/XMLSchema#nonPositiveInteger';
    const DATATYPE_SHORT                 =   'http://www.w3.org/2001/XMLSchema#short';
    const DATATYPE_STRING                =   'http://www.w3.org/2001/XMLSchema#string';
    const DATATYPE_TIME                  =   'http://www.w3.org/2001/XMLSchema#time';
    const DATATYPE_UNSIGNED_BYTE         =   'http://www.w3.org/2001/XMLSchema#unsignedByte';
    const DATATYPE_UNSIGNED_INT          =   'http://www.w3.org/2001/XMLSchema#unsignedInt';
    const DATATYPE_UNSIGNED_LONG         =   'http://www.w3.org/2001/XMLSchema#unsignedLong';
    const DATATYPE_UNSIGNED_SHORT        =   'http://www.w3.org/2001/XMLSchema#unsignedShort';

    /**
     * Returns datatypes.
     * @return array
     */
    static public function getDataTypes(): array
    {
        return self::prefixFilter('DATATYPE_');
    }

    /**
     * Returns main application URIs.
     * @return array
     */
    static public function getMainUris(): array
    {
        return self::prefixFilter('MAIN_');
    }

    /**
     * Returns concatenated datatypes.
     * @param array|null $dataTypes
     * @return array
     */
    static public function concatDataTypes(array $dataTypes = null): array
    {
        $dataTypes = $dataTypes ?: self::getDataTypes();

        foreach ($dataTypes as &$dataType)
            $dataType = self::concatDataType($dataType);

        return $dataTypes;
    }

    /**
     * Returns concatenated datatype.
     * @param string $dataType
     * @return string
     */
    static public function concatDataType(string $dataType): string
    {
        return sprintf('^^<%s>', $dataType);
    }

    /**
     * Lists constants filtered by prefix. Example: "DATATYPE_".
     * @param string $prefix
     * @return array
     */
    private function prefixFilter(string $prefix): array
    {
        $results = array();

        $constants = (new \ReflectionClass(static::class))->getConstants();
        foreach ($constants as $key => $value)
            if (strpos($key, $prefix) === 0)
                $results[$key] = $value;

        return $results;
    }
}