<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\Storer;

use App\Error\Exception\Configuration\MissingConfigurationException;
use App\Error\Exception\Format\RDF\InvalidNTriplesException;
use App\Error\Exception\Query\Sparql\SparqlQueryFailedException;
use App\Service\Ontology\AbstractOntology;
use Exception;

/**
 * Manages a set of given ontologies.
 *
 * Configurations:
 *
 * "cache_folder": Defines where the service will cache ontologies.
 * @package App\Service\Storer
 */
class StorerService
{
    /**
     * Service parameters.
     * @var array
     */
    private $parameters;

    /**
     * Application ontologies.
     * @var array
     */
    private $ontologies;

    /**
     *  Application suppliers.
     * @var array
     */
    private $suppliers;

    /**
     * OntologyService constructor.
     * @param array $suppliers Array of application suppliers. Each one must extend AbstractOntology class.
     * @param array $parameters Service configuration.
     * @throws Exception
     */
    public function __construct(array $suppliers, array $parameters = array())
    {
        $defaultParameters = array(
            "cache_folder" => "../var/cache/ontologies"
        );

        $this->parameters = array_merge($defaultParameters, $parameters);

        $this->initStorer($suppliers);
    }

    /**
     * Returns all provided ontologies.
     * @return array
     * @throws \Exception
     */
    public function getOntologies(): array
    {
        if (!$this->ontologies || count($this->ontologies) === 0)
            throw new \Exception("Error - No ontology found.");

        return $this->ontologies;
    }

    /**
     * Returns contexts for all provided ontologies.
     * @param string $key
     * @return array
     * @throws \Exception
     */
    public function getContexts(string $key = '@context'): array
    {
        $contexts = array();
        foreach ($this->ontologies as $uri => $ontology)
        {
            if (!$ontology) {
                throw new \Exception('Ontology "'.$uri.'" could not be loaded.');
            }
            $contexts = array_merge($contexts, json_decode($ontology, true)[$key]);
        }

        return $contexts;
    }

    /**
     * Returns JSON formatted ontologies.
     * @return array
     */
    public function getIndexedOntologies(): array
    {
        $ontologies = $this->getOntologies();
        foreach ($ontologies as $key => $ontology)
        {
            $ontology = json_decode($ontology, true);
            $ontologies[$key] = $ontology;
        }

        return $ontologies;
    }

    /**
     * Returns all suppliers objects.
     * @return array
     * @throws \Exception
     */
    public function getSuppliers(): array
    {
        if (!$this->suppliers || count($this->suppliers) === 0)
            throw new \Exception("Error - No provider found.");

        return $this->suppliers;
    }

    /**
     * Returns one supplier instance by its class name (without namespace).
     * @param string $name
     * @return AbstractOntology
     * @throws \Exception
     */
    public function getSupplier(string $name): AbstractOntology
    {
        if (!isset($this->suppliers[$name]))
            throw new \Exception("Error - Provider \"".$name."\" does not exist.)");

        return $this->suppliers[$name];
    }

    /**
     * Sets providers and ontologies to service.
     * @param array $suppliers
     * @return bool
     * @throws \Exception
     */
    private function initStorer(array $suppliers): bool
    {
        $this->suppliers = array();
        $this->ontologies = array();

        $count = count($suppliers);
        for ($i=0; $i<$count; $i++)
        {
            if (!$suppliers[$i] instanceof AbstractOntology)
                throw new \UnexpectedValueException(
                    "Ontology provider must be an instance of ".AbstractOntology::class.".");

            $suppliers[$i]->setParameters($this->parameters);

            if ($suppliers[$i]->getCache() === true)
            {
                if (!$this->parameters['cache_folder'])
                    throw new MissingConfigurationException(
                        "When cache is enabled, service configuration must define \"cache_folder\".");

                $suppliers[$i]->setCache($this->parameters['cache_folder']);
            }

            $providerClass = substr(strrchr(get_class($suppliers[$i]), '\\'), 1);
            $this->suppliers[$providerClass] = $suppliers[$i];

            $this->ontologies[$suppliers[$i]->getUrl()] = $suppliers[$i]->getOntology();

            return true;
        }
    }
}