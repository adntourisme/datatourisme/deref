<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology;

use Exception;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * Provides ontology from distant source or from cache if enabled.
 * @package App\Supplier\Ontology
 */
abstract class AbstractOntology implements OntologyInterface
{
    /**
     * @var FilesystemAdapter|null
     */
    protected $cache;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var array
     */
    protected $parameters;

    /**
     * {@inheritdoc}
     */
    public function __construct(bool $cache, string $url, string $namespace)
    {
        $this->cache     = $cache;
        $this->url       = $url;
        $this->namespace = $namespace;
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * {@inheritdoc}
     */
    public function setParameters(array $parameters): bool
    {
        if (count($parameters) === 0)
            throw new Exception("Must provide valid application parameters to provider");

        $this->parameters = $parameters;
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function setCache(string $folder)
    {
        $this->cache = new FilesystemAdapter($this->namespace, 0, $folder);
    }

    /**
     * {@inheritdoc}
     */
    public function getCache()
    {
        if (!is_bool($this->cache) && !$this->cache instanceof FilesystemAdapter)
            throw new Exception("Cache can only be boolean or instance of FileSystemAdapter");

        return $this->cache;
    }

    /**
     * {@inheritdoc}
     */
    public function getOntology(): string
    {
        if ($this->cache !== false)
            $ontology = $this->cacheOntology();
        else
            $ontology = $this->provideOntology();

        return $ontology;
    }

    /**
     * User defined when a new ontology provider class has to be created and injected to the ontology service.
     * @return string
     */
    abstract protected function provideOntology(): string;

    /**
     * If ontology is already cached, directly returns it from cache. If not, first caches it from provider source then returns it.
     * @return string
     */
    private function cacheOntology(): string
    {
        $ontology = $this->cache->getItem($this->getNamespace());
        if ($ontology->isHit() === false)
        {
            $ontology->set($this->provideOntology());
            $this->cache->save($ontology);
        }

        return $ontology->get();
    }
}