<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\Context;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * Manages RDF context prefixes.
 * @package App\Service\Context
 */
class ContextService
{
    /**
     * Default prefixes from https://www.w3.org/2013/json-ld-context/rdfa11.
     * @var array
     */
    private $prefixes;

    /**
     * Application defined prefixes.
     * @var array
     */
    private $appPrefixes;

    /**
     * Service parameters.
     * @var array
     */
    private $parameters;

    /**
     * Context constructor.
     * @param array|null $appPrefixes
     * @param array $parameters
     */
    public function __construct(array $appPrefixes = array(), array $parameters = array())
    {
        $defaultParameters = array(
            "cache_folder" => "../var/cache/prefixes",
        );

        $this->parameters  = array_merge($defaultParameters, $parameters);

        $this->appPrefixes = $appPrefixes;
        $this->prefixes    = $this->getPrefixes();
    }

    /**
     * Returns prefixes.
     * @param bool $cache
     * @return array
     */
    public function getPrefixes(bool $cache = true): array
    {
        if ($this->prefixes)
            return $this->prefixes;

        if ($cache === true)
            $prefixes = $this->cachePrefixes();
        else
            $prefixes = $this->providePrefixes();

        $prefixes = json_decode($prefixes, true);

        $this->setPrefixes($prefixes);

        return $this->prefixes;
    }

    /**
     * @param array $prefixes
     * @return bool
     */
    public function setPrefixes(array $prefixes): bool
    {
        $this->prefixes = $prefixes;
        return true;
    }

    /**
     * Returns full path from prefix if it exists.
     *
     * If not, returns prefix from full path if it exists.
     *
     * If not, returns null.
     * @param string $needle
     * @return mixed|null
     */
    public function get(string $needle)
    {
        $data = null;

        if (array_key_exists($needle, $this->prefixes))
        {
            $data = $this->prefixes[$needle];
        }
        else
        {
            $search = array_search($needle, $this->prefixes);
            if ($search)
                $data = $search;
        }

        return $data;
    }

    /**
     * Returns full path from prefix if it exists.
     *
     * If not, returns null.
     * @param string $prefix
     * @return mixed|null
     */
    public function getPath(string $prefix)
    {
        if (array_key_exists($prefix, $this->prefixes))
            return $this->prefixes[$prefix];

        return null;
    }

    /**
     * Returns prefix from full path if it exists.
     *
     * If not, returns null.
     * @param string $path
     * @return string|null
     */
    public function getPrefix(string $path)
    {
        return
            array_search($this->getBasePath($path), $this->getPrefixes(), true)
            ?: null;
    }

    /**
     * From full path, returns the base path of an URI (the part which can be prefixed).
     *
     * @param string $path
     * @return mixed|null
     */
    public function getBasePath(string $path)
    {
        foreach ($this->getPrefixes() as $prefix => $uri)
            if (strpos($path, $uri) === 0)
                return $uri;

        return null;
    }

    /**
     * From full path, returns the identifier of URI, i.e. everything after the prefixed part.
     *
     * Example:
     * Given the "rdf" prefix exists, `http://www.w3.org/1999/02/22-rdf-syntax-ns#type` would return `type`
     *
     * @param string $path
     * @return mixed|null
     */
    public function getIdentifier(string $path)
    {
        if ($basePath = $this->getBasePath($path))
            return substr($path, strlen($basePath));

        return null;
    }

    /**
     * From full URI, replaces the base URL by the given one.
     *
     * Example:
     * Parameters `http://www.w3.org/1999/02/22-rdf-syntax-ns#type` and `http://www.data.fr/`
     *     would return `http://www.data.fr/type`
     *
     * @param string $path
     * @param string $replace
     * @param bool $strict When true, returns null as fallback instead of the original input.
     * @return string
     */
    public function replaceBase(string $path, string $replace, bool $strict = false): string
    {
        if ($identifier = $this->getIdentifier($path))
            return $replace . $this->getIdentifier($path);

        return !$strict ? $path : null;
    }

    /**
     * Expands a prefixed URI to its full path.
     *
     * If the prefix is not defined in context, just returns the original input.
     * @param string $short
     * @param bool $strict When true, in no-match case, returns null instead of the original input.
     * @return string|null
     */
    public function expand(string $short, bool $strict = false)
    {
        $prefixes = $this->getPrefixes();
        $cut = explode(":", $short, 2);
        if (array_key_exists($cut[0], $prefixes))
            return $prefixes[$cut[0]] . $cut[1];

        return !$strict ? $short : null;
    }

    /**
     * Shortens a full path to its prefixed path.
     *
     * If no match is found, just returns the original input.
     * @param string $long
     * @param bool $strict When true, in no-match case, returns null instead of the original input.
     * @return string|null
     */
    public function shorten(string $long, bool $strict = false)
    {
        foreach ($this->getPrefixes() as $prefix => $path)
            if (strpos($long, $path) === 0)
                return $prefix . ":" . substr($long, strlen($path));

        return !$strict ? $long : null;
    }

    /**
     * Get default prefixes from source.
     * @return string
     */
    private function providePrefixes(): string
    {
        $prefixes = file_get_contents("https://www.w3.org/2013/json-ld-context/rdfa11");

        $prefixes = json_decode($prefixes, true);
        $prefixes = array_merge($prefixes['@context'], $this->appPrefixes);
        $prefixes = json_encode($prefixes, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $prefixes;
    }

    /**
     * Caches prefixes.
     * @return mixed
     */
    private function cachePrefixes()
    {
        $namespace = "rdfa11+app";
        $cache = new FilesystemAdapter($namespace, 0, $this->parameters['cache_folder']);

        $prefixes = $cache->getItem($namespace);
        if ($prefixes->isHit() === false)
        {
            $prefixes->set($this->providePrefixes());
            $cache->save($prefixes);
        }

        return $prefixes->get();
    }
}