<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\Builder;

use App\Error\Exception\Format\RDF\InvalidNTriplesException;
use App\Error\Exception\Query\Sparql\SparqlQueryFailedException;
use App\Service\Ontology\Seeker\SeekerService;
use App\Service\Ontology\Storer\StorerService;
use App\Service\Ontology\Builder\Entity\RDFGraph;
use App\Service\Ontology\Builder\Entity\RDFResource;

/**
 * Builds RDFGraph.
 * @package App\Service\Ontology\Builder
 */
class BuilderService
{
    /**
     * Storer Service
     * @var StorerService
     */
    private $storer;

    /**
     * Seeker Service
     * @var SeekerService
     */
    private $seeker;

    /**
     * Builder constructor.
     * @param SeekerService $seeker
     */
    public function __construct(SeekerService $seeker)
    {
        $this->seeker = $seeker;
    }

    /**
     * Creates RDFGraph from N-Triples string.
     *
     * If URI is provided and matches with a resource, RDFGraph will build itself upon this resource.
     * Else, it will contain a flat collection of all resources.
     * @param string $triples N-Triples.
     * @param string $uri|null Graph URI.
     * @param bool $expanded If true, RDFGraph won't restrict itself to the given triples
     *                       but will expand to ontologies' definitions.
     * @return RDFGraph
     * @throws \Exception
     */
    public function createGraph(string $triples, string $uri = null, bool $expanded = false): RDFGraph
    {
        $graph = new RDFGraph($uri, $triples);

        if ($uri !== null)
        {
            $graph->build(
                $this->buildGraphTriples($triples, $uri, true, false)
            );
        }
        else
        {
            foreach($this->indexNTriples($triples, true, false) as $key => $ts)
            {
                $resource = $this->createResource($ts, $key);
                $graph->addResource($resource);
            }
        }

        if ($expanded === true)
            $this->expandGraph($graph);

        return $graph;
    }

    /**
     * Creates RDFResource from indexed RDF array.
     * @param array $ts
     * @param string $uri
     * @return RDFResource
     */
    public function createResource(array $ts, string $uri): RDFResource
    {
        $resource = new RDFResource($uri);
        $resource = $resource->build($ts);

        return $resource;
    }

    /**
     * Expands RDFGraph with onotologies data.
     * @param RDFGraph $graph
     * @return RDFGraph
     */
    public function expandGraph(RDFGraph $graph): RDFGraph
    {
        $resources = $graph->getResources();
        foreach ($resources as $resource)
        {
            $uri = $resource->getUri();
            $ontologies = $this->seeker->findOntologies($uri);
            foreach ($ontologies as $url => $ontology)
            {
                $res = new RDFResource($uri);
                $res->build($ontology);
                $resource->addResource($res);
            }
        }

        return $graph;
    }

    /**
     * Builds recursively a hierarchical graph from flat N-Triples upon a given master URI.
     *
     * @param string $nTriples     Must be a valid N-Triples string
     *                             See specification: https://www.w3.org/TR/n-triples/
     * @param string $motherUri    The master Resource's URI.
     * @param bool   $purgeUris
     * @param bool   $returnJson
     * @return array|string|null
     */
    public function buildGraphTriples(string $nTriples, string $motherUri,
                                      bool $purgeUris = true, bool $returnJson = true
    ) {
        $indexedTriples = $this->indexNTriples($nTriples, false, false);

        /**
         * /!\ Recursive closure
         * @param string $val
         * @return array|string
         */
        $buildGraphTriples = function (string $uri) use (&$buildGraphTriples, &$indexedTriples,
                                                         &$purgeUris, &$motherUri): array {
            if (false === $this->isResourceUri($uri))
                throw new \UnexpectedValueException("Can only build graph upon RDF resource.");

            if (!isset($indexedTriples[$uri])) // The resource is not described
                return [];

            $graphTriples = [];
            // Walking through subjects
            foreach ($indexedTriples[$uri] as $propKey => $property) {
                // Walking through predicates
                foreach ($property as $value) {
                    $subjectUri  = $purgeUris === true ? $this->purgeUri($uri)     : $uri;
                    $propertyUri = $purgeUris === true ? $this->purgeUri($propKey) : $propKey;

                    // Is this some literal? So we just add a leaf
                    if (false === $this->isResourceUri($value)) {
                        $graphTriples[$subjectUri][$propertyUri][] = $value;
                    }
                    // Is this a resource? So we recursively build a new sub-branch...
                    else {
                        $valueIndex = $purgeUris === true ? $this->purgeUri($value) : $value;
                        // ... unless this is the mother resource, since we do not want infinite depth
                        if ($value === "<$motherUri>") {
                            $graphTriples[$subjectUri][$propertyUri][$valueIndex] = [$valueIndex];
                            continue;
                        }

                        $subGraph = $buildGraphTriples($value);
                        $graphTriples[$subjectUri][$propertyUri][$valueIndex] = array_shift($subGraph) ?: [];
                    }
                }
            }

            return $graphTriples;
        };

        // Calling closure
        $graphTriples = $buildGraphTriples(
            preg_match("~^<.+>$~", $motherUri) !== 1 ? "<$motherUri>" : $motherUri
        );

        return $returnJson === true
            ? json_encode($graphTriples,
                          JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            : $graphTriples;
    }

    /**
     * Formats an N-Triples string document into an exploitable flat and indexed data representation
     * (JSON or PHP array).
     *
     * @param string|array   $nTriples     Must be a valid N-Triples string or an array of triples
     *                                     A given array should look like this:
     *                                     [
     *                                         [`subject`, `predicate`, `object`],
     *                                         [`subject`, `predicate`, `object`],
     *                                         [`subject`, `predicate`, `object`]
     *                                     ]
     *                                     See specification: https://www.w3.org/TR/n-triples/
     * @param bool           $purgeUris    If true, removes "<" and ">" signs from uris.
     * @param bool           $returnJson   If true, returns a JSON string
     *                                     Else returns an associative array
     * @return array|string
     * @throws \Exception
     * @throws InvalidNTriplesException
     * @throws SparqlQueryFailedException
     */
    public function indexNTriples($nTriples, bool $purgeUris = true, bool $returnJson = true)
    {
        // Getting flat array of triples
        $triples = (function () use (&$nTriples): array {
            switch ($type = gettype($nTriples)) {
                case "string":
                    return explode("\n", $nTriples);
                case "array":
                    return $nTriples;
                default:
                    throw new \UnexpectedValueException(
                        "Expecting string or array type for N-Triples, $type given.");
            }
        })();

        // Init empty result
        $indexedTriples = array();

        // Walking through triples
        for ($i=0, $iCount = count($triples); $i<$iCount; $i++)
        {
            // We don't process empty lines
            if (empty($triples[$i]))
                continue;

            // Exploding terms
            $triples[$i] = preg_split("~[\t ]+~", $triples[$i], 3);

            // Checking for N-Triples validity
            if (count($triples[$i]) !== 3)
            {
                if (isset($triples[1][1], $triples[1][2]))
                {
                    if (strpos($triples[1][1], 'MalformedQueryException') !== false)
                        throw new SparqlQueryFailedException("Message: " . $triples[1][2]);
                    else if (strpos($triples[1][1], 'RuntimeException') !== false)
                        throw new \RuntimeException("Message: " . $triples[1][2]);
                    else if (strpos($triples[1][1], 'Exception') !== false)
                        throw new \Exception("Message: " . $triples[1][2]);
                }
                throw new InvalidNTriplesException(
                    "Line $i contains ".count($triples[$i])." elements instead of 3.");
            }
            $sLen = strlen($triples[$i][0]);
            $pLen = strlen($triples[$i][1]);
            if ($triples[$i][0][0] !== '<' || $triples[$i][0][$sLen-1] !== '>'
                || $triples[$i][1][0] !== '<' || $triples[$i][1][$pLen-1] !== '>'
            ) {
                throw new InvalidNTriplesException("Malformed triple `" . implode(' ', $triples[$i]) . "`.");
            }

            // Walking through terms
            for ($j = 0, $jCount = count($triples[$i]); $j < $jCount; $j++)
            {
                $triples[$i][$j] = trim($triples[$i][$j], " \t\n\r\0\x0B.");
                if ($purgeUris === true)
                    $triples[$i][$j] = $this->purgeUri($triples[$i][$j]);
            }

            $s = $triples[$i][0];
            $p = $triples[$i][1];
            $o = $triples[$i][2];

            $indexedTriples[$s][$p][] = $o;
        }

        return $returnJson === true
            ? json_encode($indexedTriples,
                          JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            : $indexedTriples;
    }

    /**
     * Trims whitespaces, dots and <> characters from URI.
     * @param string $uri
     * @return string
     */
    public function purgeUri(string $uri): string
    {
        if ($this->isResourceUri($uri))
            return trim($uri, " \t\n\r\0\x0B.<>");

        return $uri;
    }

    /**
     * Checks if the given term is a resource URI, based on the `^<.+>$` regex pattern.
     * This is a N-Triples compliant way of doing the thing, for example when exploding a triple in three terms.
     * This is very specific and should be kept in mind while using this method.
     *
     * Examples:
     *     `16/06/2017"^^<http://www.w3.org/2001/XMLSchema#date>` would return false
     *     `<http://www.w3.org/2001/XMLSchema#date>` would return true
     *     /!\ `http://www.w3.org/2001/XMLSchema#date` would return FALSE (tag signs are important)
     *
     * @param string $term
     * @return bool
     */
    public function isResourceUri(string $term): bool
    {
        return preg_match('~^<.+>$~', $term) === 1;
    }
}