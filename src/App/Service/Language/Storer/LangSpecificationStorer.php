<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Language\Storer;
use App\Error\Exception\Configuration\MissingConfigurationException;
use App\Service\Language\Supplier\AbstractLangSpecification;

/**
 * Class LangSpecificationStorer
 * @package App\Service\Language\Storer
 */
class LangSpecificationStorer
{
    /**
     * @var array
     */
    private $specifications = array();

    /**
     * @var array
     */
    private $suppliers      = array();

    /**
     * @var array
     */
    private $parameters     = array();

    /**
     * LangSpecificationStorer constructor.
     * @param array $suppliers
     * @param array $parameters
     */
    public function __construct(array $suppliers, array $parameters = array())
    {
        $defaultParameters = array(
            "cache_folder" => "../var/cache/lang"
        );

        $this->parameters = array_merge($defaultParameters, $parameters);

        $this->initStorer($suppliers);
    }

    /**
     * @param string $namespace
     * @return string|null
     */
    public function getSpecification(string $namespace)
    {
        return $this->specifications[$namespace] ?? null;
    }

    /**
     * @return array
     */
    public function getSpecifications(): array
    {
        return $this->specifications;
    }

    /**
     * @return array
     */
    public function getSuppliers(): array
    {
        return $this->suppliers;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return LangSpecificationStorer
     */
    public function setParameters(array $parameters): self
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * Sets providers and specifications to service.
     * @param array $suppliers
     * @return bool
     * @throws \Exception
     */
    private function initStorer(array $suppliers): bool
    {
        $count = count($suppliers);
        for ($i=0; $i<$count; $i++)
        {
            if (!$suppliers[$i] instanceof AbstractLangSpecification)
                throw new \UnexpectedValueException("Language specification provider must be an instance of ".AbstractLangSpecification::class.".");

            if ($suppliers[$i]->getCacheEnabled() === true)
            {
                if (!$this->parameters['cache_folder'])
                    throw new MissingConfigurationException("When cache is enabled, service configuration must define \"cache_folder\".");

                $suppliers[$i]->setCache($this->parameters['cache_folder']);
            }

            $providerClass = substr(strrchr(get_class($suppliers[$i]), '\\'), 1);
            $this->suppliers[$providerClass] = $suppliers[$i];

            $this->specifications[$suppliers[$i]->getNamespace()] = $suppliers[$i]->getSpecification();

            return true;
        }
    }
}