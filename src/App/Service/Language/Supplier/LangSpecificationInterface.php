<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Language\Supplier;

/**
 * Interface LangSpecificationInterface
 * @package App\Service\Language\Supplier
 */
interface LangSpecificationInterface
{
    /**
     * LangSpecificationInterface constructor.
     * @param string $namespace
     * @param bool $cacheEnabled If true, provider will use cache system. If not, it will load language from distant source each time getLanguage() is called.
     */
    public function __construct(string $namespace, bool $cacheEnabled = true);

    /**
     * Returns specification namespace.
     * @return string
     */
    public function getNamespace(): string;

    /**
     * Sets specification namespace
     * @param string $namespace
     * @return LangSpecificationInterface
     */
    public function setNamespace(string $namespace): LangSpecificationInterface;

    /**
     * @return bool
     */
    public function getCacheEnabled(): bool;

    /**
     * @param bool $cacheEnabled
     * @return LangSpecificationInterface
     */
    public function setCacheEnabled(bool $cacheEnabled): LangSpecificationInterface;

    /**
     * Returns specification
     * @return string
     */
    public function getSpecification(): string;

    /**
     * @return mixed
     */
    public function getCache();

    /**
     * @param string $folder
     * @return LangSpecificationInterface
     */
    public function setCache(string $folder): LangSpecificationInterface;
}