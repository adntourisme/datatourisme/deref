<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Language\Supplier\Rfc1766;

use App\Error\Exception\Format\RDF\InvalidJsonException;
use App\Service\Language\Supplier\AbstractLangSpecification;

/**
 * Class Rfc1766LanguageSupplier
 * @package App\Service\Language\Supplier\Rfc1766
 */
class Rfc1766LanguageSupplier extends AbstractLangSpecification
{
    /**
     * {@inheritdoc}
     */
    public function __construct(string $namespace = 'rfc_1766', bool $cacheEnabled = true)
    {
        parent::__construct($namespace, $cacheEnabled);
    }

    /**
     * {@inheritdoc}
     */
    public function provideSpecification(): string
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'rfc_1766.json';
        $list = file_get_contents($path);

        $list = json_decode($list, true);
        $list = json_encode($list, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $list;
    }
}