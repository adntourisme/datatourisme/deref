<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display\Entity;

/**
 * Class Section
 * @package App\Service\Display\Section\Entity
 */
class Section
{
    /**
     * @var string
     */
    private $uri;

    /**
     * @var string
     */
    private $label;

    /**
     * @var array
     */
    private $properties;

    /**
     * Section constructor.
     */
    public function __construct()
    {
        $this->properties = array();
    }

    /**
     * @return string|null
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return self
     */
    public function setUri(string $uri): self
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Section
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     * @return self
     */
    public function setProperties(array $properties): self
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * Can take as argument an instance of Property, or an array which will be converted in Property.
     *
     * If array is given, following keys must be defined: (string) "label", (string) "comment", (string) "uri", (array) "values".
     * @param Property|array $property
     * @return Section
     * @throws \UnexpectedValueException
     */
    public function addProperty($property): self
    {
        if ($property instanceof Property)
        {
            if ($label = $property->getLabel())
                $this->properties[$label] = $property;
            else
                $this->properties[] = $property;
        }
        else if (is_array($property))
        {
            $expectedKeys = array('label', 'comment', 'uri', 'values');
            for ($i = 0, $count = count($expectedKeys); $i < $count; ++$i)
            {
                if (!array_key_exists($expectedKeys[$i], $property))
                {
                    $keys = "\"".implode(", ", array_keys($property))."\"";
                    $errorMsg = "To be converted to instance of ".Property::class.", array must define key \"".$expectedKeys[$i]."\", following given: ".$keys.".";
                    throw new \UnexpectedValueException($errorMsg);
                }

            }
            $propObject = (new Property())
                ->setLabel($property['label'])
                ->setComment($property['comment'])
                ->setUri($property['uri'])
                ->setValues($property['values']);

            if ($label = $propObject->getLabel())
                $this->properties[$label] = $propObject;
            else
                $this->properties[] = $propObject;

        }
        else
        {
            $errorValue = gettype($property) !== 'object' ? gettype($property) : get_class($property);
            throw new \UnexpectedValueException("Given property should be array or instance of ".Property::class.", ".$errorValue." given.");
        }

        return $this;
    }

    /**
     * @return Property
     */
    public function createProperty(): Property
    {
        return new Property();
    }
}