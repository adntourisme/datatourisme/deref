<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display\Widget;

use App\Service\Ontology\Builder\Entity\RDFGraph;
use EasyRdf\RdfNamespace;
use Exception;
use Twig_Environment;

/**
 * Class AbstractWidget
 * @package App\Widget
 */
abstract class AbstractWidget implements WidgetInterface
{
    /**
     * Path to template file.
     * @var string
     */
    protected $templateFile;

    /**
     * If true, properties displayed in the widget won't be displayed in the application class list.
     * @var bool
     */
    protected $uniqueProperties;

    /**
     * RDFGraph
     * @var RDFGraph
     */
    protected $graph;

    /**
     * Template engine.
     * @var Twig_Environment
     */
    protected $engine;

    /**
     * Template data
     * @var AbstractWidgetData
     */
    protected $data;

    /**
     * AbstractWidget constructor.
     * @param string $templateFile
     * @param bool $uniqueProperties
     */
    public function __construct(string $templateFile, bool $uniqueProperties = false)
    {
        $this->templateFile = $templateFile;
        $this->uniqueProperties = $uniqueProperties;
    }

    public function getTemplateFile(): string
    {
        return $this->templateFile;
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function getGraph(): RDFGraph
    {
        if (!$this->graph)
            throw new Exception("Graph not set to widget instance.");

        return $this->graph;
    }

    /**
     * {@inheritdoc}
     */
    public function setGraph(RDFGraph $graph): bool
    {
        $this->graph = $graph;
        return true;
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function getTemplateEngine(): Twig_Environment
    {
        if (!$this->engine)
            throw new Exception("Application not set to widget instance.");

        return $this->engine;
    }

    /**
     * {@inheritdoc}
     */
    public function setTemplateEngine(Twig_Environment $engine): bool
    {
        $this->engine = $engine;
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function removeDisplayedProperties(array $propNames): int
    {
        if ($this->uniqueProperties === false)
            return 0;

        $number = 0;
        foreach ($this->getData() as $dataKey => $dataValue)
        {
            $count = count($propNames);
            for ($i=0; $i<$count; $i++)
            {
                if (RdfNamespace::shorten($propNames[$i]) === $dataKey)
                {
                    unset($propNames[$i]);
                    $propNames = array_values($propNames);
                    $count = count($propNames);
                    $number++;
                }
            }
        }
        return $number;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate(): string
    {
        $data = $this->getData();

        return $this->getTemplateEngine()->render($this->getTemplateFile(), array(
            'data' => $data
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function setData(AbstractWidgetData $data = null)
    {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    abstract public function isAvailable(): bool;

    /**
     * {@inheritdoc}
     */
    abstract public function getData();
}