<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display\Data;

use App\Service\Language\LanguageService;
use App\Service\Ontology\Context\ContextService;
use App\Supplier\RDF\RDF;

/**
 * Class DataFormatter
 * @package App\Service\Display\Data
 */
class DataFormatter
{
    const LANGTAG_REGEXP = '~(@([a-zA-Z]{2}(?:-[a-zA-Z]{2})?))$~';

    /**
     * @var ContextService
     */
    private $context;

    /**
     * @var LanguageService
     */
    private $lang;

    /**
     * @var array
     */
    private $concatDataTypes;

    /**
     * DataFormatter constructor.
     * @param ContextService $context
     * @param LanguageService $lang
     */
    public function __construct(ContextService $context, LanguageService $lang)
    {
        $this->context = $context;
        $this->lang    = $lang;

        $this->concatDataTypes = RDF::concatDataTypes();
    }

    /**
     * Very application specific. Should be tested with attention after any refactoring.
     *
     * @param  string    $propertyKey
     * @param  array  $values
     * @return array  $values
     */
    public function formatValues(string $propertyKey, array $values): array
    {
        foreach ($values as &$value)
            $value = $this->formatValue($propertyKey, $value);

        return $values;
    }

    /**
     * Very application specific. Should be tested with attention after any refactoring.
     *
     * @param string $propertyKey
     * @param string $value
     * @return string
     */
    public function formatValue(string $propertyKey, string $value): string
    {
        $str = trim($value, " \t\n\r\0\x0B\"");

        // ____ URI
        if (filter_var($str, FILTER_VALIDATE_URL) !== false)
            $str = $this->context->shorten($str);

        // ____ BOOLEAN
        else if (strpos($str, $this->concatDataTypes['DATATYPE_BOOLEAN']) !== false)
            $str = $this->formatBoolean($str, 'Oui', 'Non', true);

        // ____ FLOAT
        else if (strpos($str, $this->concatDataTypes['DATATYPE_FLOAT']) !== false)
            $str = $this->formatFloat($str, ',');

        // ____ DATE
        else if (strpos($str, $this->concatDataTypes['DATATYPE_DATE']) !== false)
            $str = $this->formatDate($str, 'Y-m-d');

        // ____ LITERAL
        else if (strpos($str, $this->concatDataTypes['DATATYPE_LITERAL']) !== false)
            $str = $this->formatLiteral($str);

        // ____ LANGUAGE
        else if (strpos($this->context->expand($propertyKey), RDF::MAIN_LANG_URI) !== false)
            $str = $this->formatLanguage($str);

        // ____ FALLBACK
        else
            $str = $this->formatLiteral($str);

        return $str;
    }

    /**
     * Formats boolean value.
     * @param string   $value
     * @param string   $trueOutput String to be displayed when value matches 'true'.
     * @param string   $falseOutput String to be displayed when value matches 'false'.
     * @param bool     $limber If true, will try a more flexible mapping with a set of possible values when given value neither is 'true' nor 'false'.
     * @param array    $trueMap In limber mode, overrides the default "true" map. Example: ['yes', '1', 'y']
     * @param array    $falseMap In limber mode, overrides the default "false" map. Example: ['no', '0', 'n']
     * @return string
     */
    public function formatBoolean(string $value, string $trueOutput, string $falseOutput,
                                  bool $limber = false, array $trueMap = [], array $falseMap = []): string
    {
        $str = $value;
        $str = $this->trimDataType($str, RDF::DATATYPE_BOOLEAN);
        $str = $this->trimDoubleQuotes($str);
        $lowStr = strtolower($str);

        if ($lowStr === 'true')
        {
            return $trueOutput;
        }
        else if ($lowStr === 'false')
        {
            return $falseOutput;
        }
        else if ($limber === true)
        {
            $tMap = !empty($trueMap)  ? $trueMap  : ['oui', 'o', 'yes', 'y', '1', 'yep', 'okay', 'ok'];
            $fMap = !empty($falseMap) ? $falseMap : ['non', 'n', 'no',  'n', '0', 'nope'];

            if (in_array($lowStr, $tMap, true))
                return $trueOutput;
            else if (in_array($lowStr, $fMap, true))
                return $falseOutput;
        }

        $str = $this->sanitizeValue($str);

        return $str;
    }

    public function formatFloat(string $value, string $separator = '.'): string
    {
        $str = $value;
        $str = $this->trimDataType($str, RDF::DATATYPE_FLOAT);
        $str = $this->trimDoubleQuotes($str);
        $str = preg_replace('~[\.,]~', $separator, $str);
        $str = $this->sanitizeValue($str);

        return $str;
    }

    /**
     * Formats date value.
     * @param string   $value
     * @param string   $format
     * @return string
     */
    public function formatDate(string $value, string $format = 'Y/m/d'): string
    {
        $str = $value;
        $str = $this->trimDataType($str, RDF::DATATYPE_DATE);
        $str = $this->trimDoubleQuotes($str);
        try {
            $str = (new \DateTime($str))->format($format);
            if (!$str)
                throw new \Exception("Invalid date format.");
        } catch (\Exception $e) {
            // ____ Do nothing
        }

        $str = $this->sanitizeValue($str);

        return $str;
    }

    /**
     * Formats language value.
     * @param string $value
     * @return string
     */
    public function formatLanguage(string $value): string
    {
        $lang = $value;
        $lang = $this->trimWhitespaces($lang);
        $lang = $this->trimDoubleQuotes($lang);
        $lang = strtolower($lang);
        $lang = $this->sanitizeValue($lang);

        return $this->lang->getLangFromCode($lang) ?? $value;

//        if ($lang = $this->lang->getLangFromCode($lang))
//            return preg_replace("~^(.+?) (\(.+?\))$~", "$1 <span id='references' class=\"text-muted\">$2</span>", $lang);
//        return $value;
    }

    /**
     * Formats literal value.
     * @param string $value
     * @param string $langClass
     * @return string
     */
    public function formatLiteral(string $value, $langClass = "label label-default"): string
    {
        $str = $value;
        $str = $this->trimDataType($str);
        $str = $this->trimDoubleQuotes($str);
        $str = $this->unescapeDoubleQuotes($str);
        $str = $this->sanitizeValue($str);
        $str = $this->handleLangTag($str, $langClass);
        $str = $this->nl2br($str);

        return $str;
    }

    /**
     * Trims datatype from value.
     *
     * Example: "16/06/2017"^^<http://www.w3.org/2001/XMLSchema#date> would be returned as "16/06/2017"
     *
     * @param string       $value
     * @param string|null  $dataType Removes any found datatype if null, else only removes the given one.
     * @return string
     */
    public function trimDataType(string $value, string $dataType = null): string
    {
        if ($dataType)
            return strstr($value, sprintf('^^<%s>', $dataType), true) ?: $value;

        preg_match("~\^\^<([^>]+)>$~", $value, $matches, PREG_OFFSET_CAPTURE);
        if (isset($matches[1]) && filter_var($matches[1][0], FILTER_VALIDATE_URL) !== false)
            return substr($value, 0, $matches[0][1]);

        return $value;
    }

    /**
     * @param string $value
     * @param string $langClass
     * @return string
     */
    public function handleLangTag(string $value, string $langClass = 'label label-default pull-right'): string {
        $str = $value;

        preg_match(self::LANGTAG_REGEXP, $value, $matches);
        if (empty($matches))
            return $value;

        $str = $this->trimLangTag($str);
        if (empty($matches[2]))
            return $str;

        $str = $this->trimDoubleQuotes($str);
        $str = $this->addLangName(
            $str, $matches[2], '<span class="' . $langClass . '">', '</span>', ' '
        );

        return $str;
    }

    /**
     * Adds language name from langcode to the given string.
     *
     * Example: Input "Hello Planet!" with langcode "en" and default
     * before/after would output "Hello Planet! [English]"
     *
     * Note that if no interval is provided, the method respects oneline
     * strings.
     * For example, if the text contains new lines, the interval between
     * text and added content will be "<br />", while it will be " " if
     * no new line is found.
     * To prevent this behaviour, just provide any interval string.
     *
     * @param string $value           The string to be processed
     * @param string $langcode        Langcode (en-gb, fr, etc.)
     * @param string $before          Caracter(s) displayed before the language name
     * @param string $after           Caracter(s) displayed after the language name
     * @param string|null $interval   Interval caracter(s) between string and added content
     * @return string
     */
    public function addLangName(string $value, string $langcode, string $before = '[',
                                   string $after = ']', string $interval = null): string
    {
        $str = $value;

        $langName = $this->lang->getLangFromCode($langcode);
        if (!$langName)
            return $str;

        $interval = (function () use ($str, $interval): string {
            if (is_string($interval))
                return $interval;
            $interval = ' ';
            $nls = ['\n', '<br>', '<br />', '<br/>'];
            for ($i = 0, $c = count($nls); $i < $c; ++$i) {
                if (strpos($str, $nls[$i]) !== false) {
                    $interval = '<br />';
                    break;
                }
            }
            return $interval;
        })();

        return $this->prependLangName($str, $langName, $interval, $before, $after);
    }

    /**
     * @param string $str
     * @param string $langName
     * @param string $interval
     * @param string $before
     * @param string $after
     * @return string
     */
    public function appendLangName(string $str, string $langName, string $interval, string $before, string $after): string
    {
        return $str . $interval . $before . $langName . $after;
    }

    /**
     * @param string $str
     * @param string $langName
     * @param string $interval
     * @param string $before
     * @param string $after
     * @return string
     */
    public function prependLangName(string $str, string $langName, string $interval, string $before, string $after): string
    {
        return $before . $langName . $after . $interval . $str;
    }

    /**
     * Removes the RDF language tag from the end of the value.
     *
     * Example: Input "Hello Planet!@en" or "Hello Planet!@en-EN" would output "Hello Planet!"
     * @param string $value
     * @return string
     */
    public function trimLangTag(string $value): string
    {
        return preg_replace(self::LANGTAG_REGEXP, '', $value);
    }

    /**
     * Trims double quotes from value.
     *
     * Double quotes are frequently met in RDF literals and may not be pertinent enough to be displayed.
     * @param string $value
     * @return string
     */
    public function trimDoubleQuotes(string $value): string
    {
        $str = $value;

        if (empty($str))
            return $str;

        if ($str[0] === '"')
            $str = substr($str, 1);

        $strLen = strlen($str);
        if ($str[$strLen-1] === '"')
            $str = substr($str, 0, $strLen-1);

        return $str;
    }

    /**
     * Trims whitespaces (\t, \n, etc.) from value.
     * @param string $value
     * @return string
     */
    public function trimWhitespaces(string $value): string
    {
        return trim($value, " \t\n\r\0\x0B");
    }

    /**
     * Unescapes double quotes.
     * @param string $value
     * @return string
     */
    public function unescapeDoubleQuotes(string $value): string
    {
        return str_replace("\\\"", "\"", $value);
    }

    /**
     * Escapes given string.
     * @param string $value
     * @return string
     */
    public function sanitizeValue(string $value): string
    {
        return htmlspecialchars($value, ENT_NOQUOTES);
    }

    /**
     * @param string $value
     * @return string
     */
    public function unsanitizeValue(string $value): string
    {
        return htmlspecialchars_decode($value);
    }

    /**
     * @param string $value
     * @return string
     */
    public function nl2br(string $value): string
    {
        return str_replace(['\r\n','\n'], '<br />', $value);
    }

    /**
     * Whashes literal from any formatting, quoting, RDF infos, etc.
     *
     * Example: `"Hello planet!"@en` would return `Hello planet!`
     *
     * @param string $value
     * @return string
     */
    public function cleanLiteral(string $value): string
    {
        $str = $value;
        $str = $this->trimDataType($str);
        $str = $this->trimLangTag($str);
        $str = $this->trimWhitespaces($str);
        $str = $this->trimDoubleQuotes($str);
        $str = $this->unescapeDoubleQuotes($str);
        $str = $this->sanitizeValue($str);

        return $str;
    }
}