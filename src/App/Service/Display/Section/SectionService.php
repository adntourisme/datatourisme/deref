<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display\Section;

use App\Service\Display\Data\DataFormatter;
use App\Service\Display\Entity\Property;
use App\Service\Display\Entity\Section;
use App\Service\Ontology\Builder\Entity\RDFResource;
use App\Service\Ontology\Context\ContextService;
use App\Service\Ontology\Hierarchy\HierarchyService;
use App\Service\Ontology\Seeker\SeekerService;
use App\Service\Ontology\Storer\StorerService;
use App\Service\Ontology\Builder\Entity\RDFGraph;
use App\Supplier\RDF\RDF;

/**
 * Class SectionService
 * @package App\Service\Display\Section
 */
class SectionService
{
    /**
     * Context Service
     * @var ContextService
     */
    public $context;

    /**
     * Hierarchy Service
     * @var HierarchyService
     */
    public $hierarchy;

    /**
     * Storer Service
     * @var StorerService
     */
    public $storer;

    /**
     * Seeker Service
     * @var SeekerService
     */
    public $seeker;

    /**
     * DataFormatter
     * @var DataFormatter
     */
    public $dataFormatter;

    /**
     * SectionService constructor.
     * @param ContextService $context
     * @param HierarchyService $hierarchy
     * @param StorerService $storer
     * @param SeekerService $seeker
     * @param DataFormatter $dataFormatter
     */
    public function __construct(ContextService $context, HierarchyService $hierarchy, StorerService $storer,
                                SeekerService $seeker, DataFormatter $dataFormatter
    ) {
        $this->context       = $context;
        $this->hierarchy     = $hierarchy;
        $this->storer        = $storer;
        $this->seeker        = $seeker;
        $this->dataFormatter = $dataFormatter;
    }

    /**
     * Returns displayable sections according to an RDFGraph.
     * @param RDFGraph $graph
     * @return array
     * @throws \Exception
     */
    public function getAvailableSections(RDFGraph $graph): array
    {
        $sections = array();
        $hierarchy = $this->buildClassHierarchy($graph);
        $propUris = $graph->getMasterResource()->getUris();

        foreach($hierarchy as $class)
        {
            if (!isset($class['label']) || !is_string($class['label']))
                continue;

            // ____ Closure
            $buildSections = function (string $classUri, array $properties) use (&$sections, &$graph, &$propUris) {
                foreach($properties as $property)
                {
                    $propertyKey = $this->context->expand($property);
                    if (!in_array($propertyKey, $propUris, true))
                        continue;

                    $classLabel = $this->seeker->findClassLabel($classUri)
                        ?? $this->context->shorten($classUri);
                    $propertyLabel = $this->seeker->findClassLabel($property)
                        ?? $this->context->shorten($property);

                    if (!isset($sections[$classLabel]))
                    {
                        $sections[$classLabel] = (new Section())
                            ->setUri($this->context->expand($classUri))
                            ->setLabel($classLabel);
                    }
                    $sections[$classLabel]
                        ->addProperty(
                            (new Property())
                            ->setLabel($propertyLabel)
                            ->setComment($this->seeker->findClassComment($property))
                            ->setUri($this->context->expand($property))
                            ->setValues((function () use (&$graph, &$propertyKey) {
                                $resource = $graph->getMasterResource()->getResource($propertyKey);
                                return $resource ? $resource->getAllValues() : null;
                            })())
                        )
                    ;
                }
            };
            // ________

            // ____ Calling Closure
            if (isset($class['subClasses']))
                $buildSections($class['uri'], $class['subClasses']);
            if (isset($class['directProperties']))
                $buildSections($class['uri'], $class['directProperties']);
            // ________
        }

        return $sections;
    }

    /**
     * Sorts sections's properties based on `hasPriority` property.
     * @param array $sections
     * @return array
     */
    public function prioritizeSections(array $sections): array
    {
        $prioSections = $sections;
        foreach ($prioSections as &$section) {
            $properties = $section->getProperties();
            uasort($properties, function ($a, $b) {
                $aPriority = $this->seeker->findClassPriority($a->getUri());
                $bPriority = $this->seeker->findClassPriority($b->getUri());
                if ($aPriority === $bPriority)
                    return 0;

                return ($aPriority > $bPriority) ? -1 : 1;
            });
            $section->setProperties($properties);
        }
        return $prioSections;
    }

    /**
     * Creates Section entity.
     * @return Section
     */
    public function createSection(): Section
    {
        return new Section();
    }

    /**
     * Creates Property entity.
     * @return Property
     */
    public function createProperty(): Property
    {
        return new Property();
    }

    /**
     * Builds a hierarchy of classes upon RDFGraph and ontology.
     * @param RDFGraph $graph
     * @return array
     */
    private function buildClassHierarchy(RDFGraph $graph): array
    {
        $classes = array();
        $resources = $graph->getMasterResource()->getResources() ?? array();

        if (isset($resources[RDF::MAIN_TYPE_URI])) {
            foreach ($resources[RDF::MAIN_TYPE_URI]->getAllValues() as $value) {
                $classes[] = (function () use (&$value): string {
                    switch ($type = gettype($value)) {
                        case 'string':
                            return $this->context->shorten($value);
                        case 'object':
                            if ($value instanceof RDFResource)
                                return $this->context->shorten($value->getUri());
                            else
                                throw new \UnexpectedValueException(
                                    "Instance of ".RDFResource::class." expected, ".get_class($value)." given.");
                        default:
                            throw new \UnexpectedValueException("String or object expected, $type given.");
                }
                })();
            }
        }

        return $this->hierarchy->getHierarchy($classes, false);
    }
}