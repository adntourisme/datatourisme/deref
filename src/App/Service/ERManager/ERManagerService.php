<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\ERManager;

use EasyRdf\Graph;
use EasyRdf\RdfNamespace;
use EasyRdf\Sparql\Result;
use Exception;

/**
 * Interface to manage EasyRdf through multiple actions such as serialisation.
 * @package App\Service\ERManager
 */
class ERManagerService
{
    /**
     * Adds custom namespaces to EasyRdf prefixes collection.
     * @param array $namespaces
     * @return void
     */
    public function addNamespaces(array $namespaces)
    {
        foreach ($namespaces as $prefix => $path)
            RdfNamespace::set($prefix, (string) $path);
    }

    /**
     * Fixes some EasyRdf unexpected behavior, converting any Sparql\Result object into a Graph object, or doing nothing if data is already Graph.
     * @param mixed $data Data to be fixed.
     * @param string $uri Graph uri.
     * @return Graph
     * @throws Exception
     */
    public function graphFormat($data, string $uri): Graph
    {
        if ($data instanceof Result)
        {
            $graph = new Graph($uri);
            foreach($data as $row)
                $graph->add($row->subject, $row->predicate, $row->object);
        }
        else if ($data instanceof Graph)
        {
            $graph = $data;
        }
        else
        {
            throw new Exception("Wrong data type.");
        }

        return $graph;
    }

    /**
     * Returns a serialised graph according to the given format, or returns null if given format met no match.
     *
     * Possible formats:
     *
     * JSON-LD => "json-ld", "jsonld"
     *
     * JSON => "json"
     *
     * RDF/XML => "rdf-xml", "rdfxml", "rdf", "xml"
     *
     * Turtle => "ttl", "turtle"
     *
     * N-Triples => "n-triples", "ntriples", "nt"
     * @param Graph $graph Graph to be serialised.
     * @param string $format Target format. See method description.
     * @return null|string
     * @throws \Exception
     */
    public function format(Graph $graph, string $format)
    {
        $format = strtolower($format);
        $data = null;

        if ($format === 'json-ld' || $format === 'jsonld')
            $data = $this->jsonLdFormat($graph);

        else if ($format === 'json')
            $data = $this->jsonFormat($graph);

        else if ($format === 'rdf-xml' || $format === 'rdfxml' || $format === 'rdf' || $format === 'xml')
            $data = $this->xmlFormat($graph);

        else if ($format === 'ttl' || $format === 'turtle')
            $data = $this->turtleFormat($graph);

        else if ($format === 'n-triples' || $format === 'ntriples' || $format === 'nt')
            $data = $this->nTriplesFormat($graph);

        if (!is_string($data) && !is_null($data))
            throw new \UnexpectedValueException("Can only return string or null.");

        return $data;
    }

    /**
     * Forces download as file of a given string.
     * @param string $response Response to be downloaded.
     * @param string $fileName File name.
     * @param string $fileExtension File extension.
     * @return string
     */
    public function downloadResponse(string $response, string $fileName, string $fileExtension): string
    {
        $file = $fileName.'.'.$fileExtension;
        header('Content-Disposition: attachment; filename="'.$file.'"');
        header('Content-Type: text/plain');
        header('Content-Length: '.strlen($response));
        header('Connection: close');

        return $response;
    }

    /**
     * Serialises Graph to JSON-LD.
     * @param Graph $graph
     * @return string
     */
    private function jsonLdFormat(Graph $graph): string
    {
        $data = $graph->serialise('jsonld');
        $data = json_decode($data);
        $data = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        return $data;
    }

    /**
     * Serialises Graph to JSON.
     * @param Graph $graph
     * @return string
     */
    private function jsonFormat(Graph $graph): string
    {
        $data = $graph->serialise('json');
        $data = json_decode($data);
        $data = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        return $data;
    }

    /**
     * Serialises Graph to RDF/XML.
     * @param Graph $graph
     * @return string
     */
    private function xmlFormat(Graph $graph): string
    {
        $data = $graph->serialise('rdfxml');

        return $data;
    }

    /**
     * Serialises Graph to Turtle.
     * @param Graph $graph
     * @return string
     */
    private function turtleFormat(Graph $graph): string
    {
        $data = $graph->serialise('turtle');

        return $data;
    }

    /**
     * Serialises Graph to N-Triples.
     * @param Graph $graph
     * @return string
     */
    private function nTriplesFormat(Graph $graph): string
    {
        $data = $graph->serialise('ntriples');

        return $data;
    }
}