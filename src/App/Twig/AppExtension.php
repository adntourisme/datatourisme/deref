<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Twig;

use App\Service\Config\ConfigService;
use App\Service\Display\Data\DataFormatter;
use App\Service\Language\LanguageService;
use App\Service\Ontology\Builder\Entity\RDFResource;
use App\Service\Ontology\Context\ContextService;
use App\Service\Ontology\Seeker\SeekerService;

/**
 * Class AppExtension
 * @package App\Twig
 */
class AppExtension extends \Twig_Extension
{
    /**
     * @var ContextService
     */
    private $contextService;

    /**
     * @var ConfigService
     */
    private $configService;

    /**
     * @var LanguageService
     */
    private $languageService;

    /**
     * @var SeekerService
     */
    private $seekerService;

    /**
     * @var DataFormatter
     */
    private $dataFormatter;

    /**
     * AppExtension constructor.
     * @param ContextService $contextService
     * @param ConfigService $configService
     */
    public function __construct(ContextService $contextService, ConfigService $configService,
                                LanguageService $languageService, SeekerService $seekerService)
    {
        $this->contextService = $contextService;
        $this->configService = $configService;
        $this->displayService = $languageService;
        $this->seekerService = $seekerService;
        $this->dataFormatter = new DataFormatter($contextService, $languageService);
    }

    /**
     * @param string $str
     * @return string
     */
    public function shorten(string $str): string
    {
        return $this->contextService->shorten($str);
    }

    /**
     * @param string $str
     * @return string
     */
    public function expand(string $str): string
    {
        return $this->contextService->expand($str);
    }
//
//    /**
//     * The PHP `strip_tags` function (http://php.net/manual/fr/function.strip-tags.php) internally used by
//     * the Twig `|striptags` filter (https://twig.symfony.com/doc/2.x/filters/striptags.html) might
//     * lead to very unsafe outputs since it does not handle attributes.
//     *
//     * Here we only allow clean tags without any attributes.
//     *
//     * Example:
//     *     The template...
//     *         `<strong><em class="lalala">Hello Planet</strong></em><br /> | safetags('strong', 'em', 'br')`
//     *     would output...
//     *         `<strong>Hello Planet</strong><br />`
//     *
//     * @param   string    $str        The subject of the escape
//     * @param   string[]  $whitelist  The list of ALLOWED tags, just by names, without <>/ signs (ex: 'small', 'h2')
//     * @return  string                The escaped string
//     */
//    public function safetags(string $str, string ...$whitelist): string
//    {
//        $tags = implode("|", array_map(function (string $n): string { return preg_quote($n); }, $whitelist));
/*        return preg_replace("~(<(?!\/?(?:$tags)).+?(?:\s+.+?\/?)?>)~i", "", $str);*/
//    }

    /**
     * @param string $uri
     * @return null|string
     */
    public function findClassLabel(string $uri)
    {
        return $this->seekerService->findClassLabel($uri);
    }

    /**
     * @param string $uri
     * @return null|string
     */
    public function findClassComment(string $uri)
    {
        return $this->seekerService->findClassComment($uri);
    }

    /**
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @return string
     */
    public function strReplace(string $search, string $replace, string $subject): string
    {
        return str_replace($search, $replace, $subject);
    }

    /**
     * @param \DateTime $date
     * @return string
     */
    public function datize(\DateTime $date, bool $year = true): string
    {
        $months = [
            '01' => 'janvier',   '02' => 'février', '03' => 'mars',     '04' => 'avril',
            '05' => 'mai',       '06' => 'juin',    '07' => 'juillet',  '08' => 'août',
            '09' => 'septembre', '10' => 'octobre', '11' => 'novembre', '12' => 'décembre'
        ];

        $d = ($j = $date->format('j')) === '1' ? '1er' : $j;
        $m = $months[$date->format('m')];
        $y = $year ? $date->format('Y') : '';

        return "$d $m $y";
    }

    /**
     * @param string $str
     * @param int $start
     * @param int|null $length
     * @return bool|string
     */
    public function substr(string $str, int $start, int $length = null)
    {
        return $length !== null
            ? substr($str, $start, $length)
            : substr($str, $start)
        ;
    }

    /**
     * Replaces base URI of given URI by application "data_path" parameter.
     * @param string $str
     * @return string
     */
    public function datapathize(string $str): string
    {
        if (!$this->configService->getParameter('relative_links'))
            return $str;

        if (!$this->isLinkable($str))
            return $str;

        $dataPath = $this->configService->getParameter('data_path');
        $long = $this->contextService->expand($str);
        $basePath = $this->contextService->getBasePath($long);
        if (strpos($basePath, $dataPath) === 0)
            return $this->contextService->replaceBase($long, '/' . substr($basePath, strlen($dataPath)));

        return $str;
    }

    /**
     * @param string|null $str
     * @return null|string
     */
    public function langize(string $str = null)
    {
        if ($str === null)
            return null;

        return $this->dataFormatter->handleLangTag($str);
    }

    /**
     * @param string $propertyKey
     * @param string $value
     * @return string
     */
    public function formatValue(string $propertyKey, string $value = null)
    {
        if ($value === null)
            return null;

        return $this->dataFormatter->formatValue($propertyKey, $value);
    }

    /**
     * @param string|null $str
     * @return null|string
     */
    public function formatLanguage(string $str = null)
    {
        if ($str === null)
            return null;

        return $this->dataFormatter->formatLanguage($str);
    }

    /**
     * @param string|null $str
     * @param string $langClass
     * @return null|string
     */
    public function formatLiteral(string $str = null, string $langClass = 'label label-default pull-right')
    {
        if ($str === null)
            return null;

        return $this->dataFormatter->formatLiteral($str, $langClass);
    }

    /**
     * @param string|null $str
     * @return null|string
     */
    public function cleanLiteral(string $str = null)
    {
        if ($str === null)
            return null;

        return $this->dataFormatter->cleanLiteral($str);
    }

    /**
     * @param $value
     * @return bool
     */
    public function isResource($value): bool
    {
        return $value instanceof RDFResource;
    }

    /**
     * @param $value
     * @return bool
     */
    public function isString($value): bool
    {
        return is_string($value);
    }

    /**
     * @param string $str
     * @return bool
     */
    public function isLinkable(string $str): bool
    {
        return filter_var(
            $this->contextService->expand($str),
            FILTER_VALIDATE_URL
        );
    }

    /**
     * @param string $str
     * @return bool
     */
    public function isEmailable(string $str): bool
    {
        return filter_var(
            $str,
            FILTER_VALIDATE_EMAIL
        );
    }
}