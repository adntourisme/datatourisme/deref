<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App;

use App\Service\ERManager\ERManagerService;
use App\Service\Ontology\Builder\BuilderService;
use App\Service\Config\ConfigService;
use App\Service\Display\DisplayService;
use App\Service\Ontology\Context\ContextService;
use App\Service\Ontology\Hierarchy\HierarchyService;
use App\Service\Language\LanguageService;
use App\Service\Ontology\Seeker\SeekerService;
use App\Service\Ontology\Storer\StorerService;
use App\Supplier\Ontology\Datatourisme;
use App\Twig\AppExtension;
use Monolog\Logger;
use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Symfony\Component\Translation\Loader\PoFileLoader;

use App\Controller\IndexController;
use App\Controller\ReferenceController;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\DebugClassLoader;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

/**
 * Main application container.
 * @package App
 */
class App extends Application
{
    /**
     * App constructor.
     * @param bool $dev Should be false (default) for prod.
     */
    public function __construct(bool $dev = false)
    {
        parent::__construct();

        $this->initServices();
        $this->initRoutes();
//        $this->initDev();
    }

    /**
     * Initializing application services.
     * @return bool
     */
    private function initServices(): bool
    {
        /**
         * Session
         */
        $this->register(new SessionServiceProvider());

        /**
         * Monolog
         */
        $this->register(new MonologServiceProvider(), array(
            'monolog.logfile' => '../var/logs/logs',
            'monolog.level'   => Logger::WARNING
        ));

        /**
         * HttpFragment
         */
        $this->register(new HttpFragmentServiceProvider());

        /**
         * translate
         */
        $this->register(new LocaleServiceProvider());
        $this->register(new TranslationServiceProvider(), array(
            'locale' => 'fr',
            'locale_fallbacks' => array('fr'),
        ));
        $this->extend('translator', function($translator, $app) {
            $translator->addLoader('po', new PoFileLoader());
            $translator->addResource('po', '../common/datatourisme/webapp-bundle/Resources/translations/messages.en.po', 'en');
            $translator->addResource('po', '../common/datatourisme/webapp-bundle/Resources/translations/messages.fr.po', 'fr');
            return $translator;
        });

        /**
         * @return ConfigService
         */
        $this['config'] = function(): ConfigService { return new ConfigService("../app/config/parameters.yml"); };

        /**
         * @return StorerService
         */
        $this['ontology.storer'] = function(): StorerService {
            return new StorerService(array(
                new Datatourisme(true)
            ));
        };

        /**
         * @return ContextService
         */
        $this['ontology.context'] = function(): ContextService {
            return new ContextService(
                $this['ontology.storer']->getcontexts()
            );
        };

        /**
         * @return LanguageService
         */
        $this['service.language'] = function (): LanguageService {
            return new LanguageService();
        };

        /**
         * @return SeekerService
         */
        $this['ontology.seeker'] = function(): SeekerService {
            return new SeekerService(
                $this['ontology.storer'],
                $this['ontology.context']
            );
        };


        /**
         * Twig
         */
        $paths = [
            '../app/Resources/views',
            '../common/datatourisme/webapp-bundle/Resources/views' => 'DatatourismeWebApp',
        ];
        $this->register(new TwigServiceProvider());
        $this['twig.options'] = array('cache' => __DIR__.'/../../var/cache/twig');
        $this['twig.loader.filesystem'] = (function() use (&$paths): \Twig_Loader_Filesystem {
            $fileSystem = new \Twig_Loader_Filesystem();
            foreach ($paths as $key => $val) {
                if (is_string($key)) {
                    $fileSystem->addPath($key, $val);
                } else {
                    $fileSystem->addPath($val);
                }
            }
            return $fileSystem;
        })();
        $contextService =& $this['ontology.context'];
        $configService =& $this['config'];
        $languageService =& $this['service.language'];
        $seekerService =& $this['ontology.seeker'];
        $this->extend('twig', function(\Twig_Environment $twig, App $app)
                              use (&$contextService, &$configService, &$languageService, &$seekerService): \Twig_Environment {
            $appExtension = new AppExtension($contextService, $configService, $languageService, $seekerService);
            $twig->addExtension($appExtension);
            $twig->addFilter('expand', new \Twig_Filter_Function(array($appExtension, 'expand')));
            $twig->addFilter('shorten', new \Twig_Filter_Function(array($appExtension, 'shorten')));
            $twig->addFilter('datize', new \Twig_Filter_Function(array($appExtension, 'datize')));
            $twig->addFilter('substr', new \Twig_Filter_Function(array($appExtension, 'substr')));
            $twig->addFilter('datapathize', new \Twig_Filter_Function(array($appExtension, 'datapathize')));
            $twig->addFilter('langize', new \Twig_Filter_Function(array($appExtension, 'langize'), array('is_safe' => array('html'))));
            $twig->addFilter('format_literal', new \Twig_Filter_Function(array($appExtension, 'formatLiteral'), array('is_safe' => array('html'))));
            $twig->addFilter('format_language', new \Twig_Filter_Function(array($appExtension, 'formatLanguage'), array('is_safe' => array('html'))));
            $twig->addFilter('format_value', new \Twig_Filter_Function(array($appExtension, 'formatValue'), array('is_safe' => array('html'))));
            $twig->addFilter('clean_literal', new \Twig_Filter_Function(array($appExtension, 'cleanLiteral'), array('is_safe' => array('html'))));
            $twig->addFunction('is_linkable', new \Twig_Function_Function(array($appExtension, 'isLinkable')));
            $twig->addFunction('is_emailable', new \Twig_Function_Function(array($appExtension, 'isEmailable')));
            $twig->addFunction('find_class_label', new \Twig_Function_Function(array($appExtension, 'findClassLabel')));
            $twig->addFunction('find_class_comment', new \Twig_Function_Function(array($appExtension, 'findClassComment')));
            $twig->addFunction('str_replace', new \Twig_Function_Function(array($appExtension, 'strReplace')));
            $twig->addFunction('preg_replace', new \Twig_Function_Function(array($appExtension, 'pregReplace')));
            $twig->addFunction('is_resource', new \Twig_Function_Function(array($appExtension, 'isResource')));
            $twig->addFunction('is_string', new \Twig_Function_Function(array($appExtension, 'isString')));
            $twig->addGlobal('data_path', $this['config']->getParameter('data_path'));

            return $twig;
        });
        $this->register(new AssetServiceProvider());

        $this->initDev();

        /**
         * @return BuilderService
         */
        $this['ontology.builder'] = function(): BuilderService {
            return new BuilderService(
                $this['ontology.seeker']
            );
        };

        /**
         * @return HierarchyService
         */
        $this['ontology.hierarchy'] = function(): HierarchyService {
            return new HierarchyService(
                $this['ontology.storer'],
                $this['ontology.seeker'],
                $this['ontology.context']
            );
        };

        /**
         * @return ERManagerService
         */
        $this['service.er_manager'] = function(): ERManagerService { return new ERManagerService(); };

        /**
         * @return DisplayService
         */
        $this['service.display'] = function(): DisplayService {
            return new DisplayService(
                $this['ontology.context'],
                $this['ontology.hierarchy'],
                $this['ontology.storer'],
                $this['ontology.seeker'],
                $this['service.language'],
                $this['twig'],
                $this['config']
            );
        };

        return true;
    }

    /**
     * Initializing application routes.
     * @return bool
     */
    private function initRoutes(): bool
    {
        $this->mount('/references/', new ReferenceController());
        $this->mount('/', new IndexController());

        return true;
    }

    /**
     * Initializing development environment.
     * @param bool $forceDevMode If true, will init dev mode regardless to configuration parameter
     * @return bool
     */
    private function initDev(bool $forceDevMode = false): bool
    {
        if ((bool) $this['config']->getParameter('dev_mode') === true || $forceDevMode === true)
        {
            $this['twig.loader.filesystem']->addPath(
                '../vendor/symfony/web-profiler-bundle/Resources/views',
                'WebProfiler'
            );
            try
            {
                if (!class_exists(WebProfilerServiceProvider::class))
                    throw new \Exception("Dev dependencies required in order to run dev mode. \n
                                         While in prod, should pass \"FALSE\" to App.");

                $this['debug'] = true;

                $this->register(new ServiceControllerServiceProvider());
                $this['profiler.cache_dir'] = '../var/cache/dev/profiler';

//                $this->register(
//                    new WebProfilerServiceProvider(), array(
////                        'profiler.cache_dir'    => '../var/cache/dev/profiler',
//                        'profiler.mount_prefix' => '/_profiler',
//                        'web_profiler.debug_toolbar.enable' => true,
//                    )
//                );

                Debug::enable();
                ErrorHandler::register();
                ExceptionHandler::register();
                DebugClassLoader::enable();

                return true;
            }
            catch (\Exception $e)
            {
                $this['monolog']->warning($e->getMessage());
            }
        }

        return false;
    }
}